column(width = 12, align = "center",

       selectInput(inputId = "worker.places", label = "Mesta:",
                   multiple = TRUE, choices = c("Novi Sad", "Beograd", "Nis")),

       selectInput(inputId = "worker.years", label = "Godine:",
                   multiple = TRUE, choices = seq.int(1389, 2389, 1)),

       actionButton(inputId = "worker.submit", label = "Generisi izvestaje"),
       downloadButton(outputId = "worker.download", label = "Skini izvestaje"),
       actionButton(inputId = "worker.refresh", label = "Ponovo ucitaj podatke"))
